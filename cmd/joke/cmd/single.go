package cmd

import (
	"fmt"

	"github.com/rs/zerolog"
	"github.com/spf13/cobra"
	"gitlab.com/eekwong/joke/cmd/joke/service"
)

// singleCmd represents the single command
var singleCmd = &cobra.Command{
	Use:   "single",
	Short: "Get a single joke",
	Long:  `Get a single joke`,
	Run: func(cmd *cobra.Command, args []string) {
		// get the firstname and lastname from cmdline
		firstname, _ := cmd.Flags().GetString("firstname")
		lastname, _ := cmd.Flags().GetString("lastname")

		verbose, _ := cmd.Flags().GetBool("verbose")
		if verbose {
			zerolog.SetGlobalLevel(zerolog.DebugLevel)
		}

		joke, err := service.GetFunnyJoke(firstname, lastname, "", "")
		if err != nil {
			fmt.Println(err)
			return
		}

		fmt.Println(joke)
	},
}

func init() {
	rootCmd.AddCommand(singleCmd)

	singleCmd.Flags().String("firstname", "", "First name")
	singleCmd.Flags().String("lastname", "", "Last name")
}

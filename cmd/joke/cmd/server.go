package cmd

import (
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gitlab.com/eekwong/joke/cmd/joke/service"
	"gitlab.com/eekwong/joke/pkg/lifecycle"
)

// serverCmd represents the server command
var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "Starts the server",
	Long:  `Starts the server`,
	Run: func(cmd *cobra.Command, args []string) {
		cached, _ := cmd.Flags().GetInt("cached")
		workers, _ := cmd.Flags().GetInt("workers")
		ratelimit, _ := cmd.Flags().GetInt("workers")
		listen, _ := cmd.Flags().GetString("listen")

		verbose, _ := cmd.Flags().GetBool("verbose")
		if verbose {
			zerolog.SetGlobalLevel(zerolog.DebugLevel)
		}

		oauthDomain, _ := cmd.Flags().GetString("sso-domain")
		oauthClientID, _ := cmd.Flags().GetString("sso-client-id")
		oauthClientSecret, _ := cmd.Flags().GetString("sso-client-secret")

		log.Info().
			Int("cached", cached).
			Int("workers", workers).
			Msg("starting joke server...")
		lifecycle.NewApplication(
			service.NewJokeServer(
				listen,
				cached,
				workers,
				ratelimit,
				oauthDomain,
				oauthClientID,
				oauthClientSecret,
				verbose),
		).Run()
	},
}

func init() {
	rootCmd.AddCommand(serverCmd)

	serverCmd.Flags().Int("cached", 20, "number of names/jokes prefetched, max: 500")
	serverCmd.Flags().Int("workers", 10, "number of workers to fetch jokes, max: cached/2")
	serverCmd.Flags().Int("ratelimit", 500, "ratelimit to number of requests per second")
	serverCmd.Flags().String("listen", ":8080", "HTTP listen address")

	serverCmd.Flags().String("sso-domain", "", "Google oauth authorized domain")
	serverCmd.Flags().String("sso-client-id", "", "Google oauth client ID")
	serverCmd.Flags().String("sso-client-secret", "", "Google oauth client secret")
}

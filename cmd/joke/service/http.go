package service

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/rs/zerolog/log"
)

var (
	nameAPILatency = promauto.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "joke_name_api_request_latency_ms",
			Help:    "Histogram for the request latency in the external Name API",
			Buckets: []float64{500, 750, 1000, 1500, 2500, 5000, 10000},
		},
		[]string{},
	)
	jokeAPILatency = promauto.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "joke_joke_api_request_latency_ms",
			Help:    "Histogram for the request latency in the external Joke API",
			Buckets: []float64{50, 100, 200, 300, 500, 1000, 5000, 10000},
		},
		[]string{},
	)
)

func getNameFromAPI(nameAPI string, out chan<- PersonError) {
	// disable the security check due to a self signed cert
	defer close(out)

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	httpClient := http.Client{
		Timeout:   time.Second * 10,
		Transport: tr,
	}

	start := time.Now()
	res, err := httpClient.Get(nameAPI)
	if err != nil {
		out <- PersonError{err: err}
		return
	}
	defer res.Body.Close()

	elapsed := time.Since(start)
	log.Debug().
		Dur("elapsed", elapsed).
		Msg("getNameFromAPI")
	nameAPILatency.
		With(prometheus.Labels(make(map[string]string))).
		Observe(float64(elapsed) / float64(time.Millisecond))

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		out <- PersonError{err: err}
		return
	}

	p := Person{}
	if err := json.Unmarshal(body, &p); err != nil {
		out <- PersonError{err: err}
		return
	}

	out <- PersonError{p: p}
}

func getJokeFromAPI(jokeAPI string, out chan<- JokeResultError) {
	defer close(out)

	httpClient := http.Client{
		Timeout: time.Second * 10,
	}

	start := time.Now()
	res, err := httpClient.Get(jokeAPI)
	if err != nil {
		out <- JokeResultError{err: err}
		return
	}
	defer res.Body.Close()

	elapsed := time.Since(start)
	log.Debug().
		Dur("elapsed", elapsed).
		Msg("getJokeFromAPI")
	jokeAPILatency.
		With(prometheus.Labels(make(map[string]string))).
		Observe(float64(elapsed) / float64(time.Millisecond))

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		out <- JokeResultError{err: err}
		return
	}

	jr := JokeResult{}
	if err := json.Unmarshal(body, &jr); err != nil {
		out <- JokeResultError{err: err}
		return
	}

	out <- JokeResultError{jr: jr}
}

func combineJokeWithName(joke string, person Person) string {
	tmp := strings.ReplaceAll(joke, "##firstname##", person.FirstName)
	return strings.ReplaceAll(tmp, "##lastname##", person.LastName)
}

// GetFunnyJoke gets a joke with random name
func GetFunnyJoke(firstname, lastname, nameAPI, jokeAPI string) (string, error) {
	if nameAPI == "" {
		nameAPI = "https://api.namefake.com"
	}
	if jokeAPI == "" {
		jokeAPI = "http://api.icndb.com/jokes/random?firstName=%23%23firstname%23%23&lastName=%23%23lastname%23%23"
	}

	start := time.Now()
	defer func() {
		log.Debug().
			Dur("elapsed", time.Since(start)).
			Msg("GetFunnyJoke")
	}()

	// Get Joke
	outJoke := make(chan JokeResultError)
	go getJokeFromAPI(jokeAPI, outJoke)

	var personError PersonError
	if firstname == "" || lastname == "" {
		// Get Name if user has not provided both names
		outName := make(chan PersonError)
		go getNameFromAPI(nameAPI, outName)

		personError = <-outName
		if personError.err != nil {
			return "", personError.err
		}

		if firstname != "" {
			personError.p.FirstName = firstname
		}
		if lastname != "" {
			personError.p.LastName = lastname
		}
	} else {
		personError = PersonError{
			p: Person{
				Name:      fmt.Sprintf("%s %s", firstname, lastname),
				FirstName: firstname,
				LastName:  lastname,
			},
		}
	}

	jokeResultError := <-outJoke
	if jokeResultError.err != nil {
		return "", jokeResultError.err
	}

	return combineJokeWithName(jokeResultError.jr.Value.Joke, personError.p), nil
}

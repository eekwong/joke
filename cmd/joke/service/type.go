package service

import (
	"encoding/json"
	"strings"
)

// Person is the person struct returns from https://api.namefake.com API
type Person struct {
	Name      string `json:"name"`
	FirstName string
	LastName  string
}

// UnmarshalJSON also calculates the FirstName and LastName of the Person
func (p *Person) UnmarshalJSON(data []byte) error {
	type tmp Person
	if err := json.Unmarshal(data, (*tmp)(p)); err != nil {
		return err
	}
	nameSlice := strings.Split(p.Name, " ")
	if len(nameSlice) > 0 {
		p.FirstName = nameSlice[0]
		p.LastName = nameSlice[len(nameSlice)-1]
	}
	return nil
}

// PersonError is a tuple of Person and error
type PersonError struct {
	p   Person
	err error
}

// JokeValue holds the Joke value
type JokeValue struct {
	Joke string `json:"joke"`
}

// JokeResult is the data from the http://api.icndb.com API
type JokeResult struct {
	JokeType string    `json:"type"`
	Value    JokeValue `json:"value"`
}

// JokeResultError is a tuple of JokeResult and error
type JokeResultError struct {
	jr  JokeResult
	err error
}

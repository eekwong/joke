// +build !ci

package service

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"sync/atomic"
	"testing"
	"time"

	"github.com/facebookarchive/freeport"
	"github.com/rs/zerolog"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func setup(t *testing.T,
	ratelimit int,
	nameAPIResponse func(w http.ResponseWriter, req *http.Request),
	jokeAPIResponse func(w http.ResponseWriter, req *http.Request),
) (string, *int64, func(t *testing.T)) {
	zerolog.SetGlobalLevel(zerolog.Disabled)

	// get a free port
	port, err := freeport.Get()
	require.NoError(t, err)
	listen := fmt.Sprintf(":%d", port)

	var reqCount int64

	js := NewJokeServer(listen, 0, 1, ratelimit, "", "", "", false)

	// mock the httptest servers for nameAPI and jokeAPI
	nameAPIServer := httptest.NewTLSServer(http.HandlerFunc(
		func(w http.ResponseWriter, req *http.Request) {
			atomic.AddInt64(&reqCount, 1)
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(200)
			w.Write([]byte(`{"name": "Anastacio Sauer Jr."}`))
		},
	))
	if nameAPIResponse != nil {
		nameAPIServer = httptest.NewTLSServer(http.HandlerFunc(nameAPIResponse))
	}
	js.nameAPI = nameAPIServer.URL

	jokeAPIServer := httptest.NewServer(http.HandlerFunc(
		func(w http.ResponseWriter, req *http.Request) {
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(200)
			w.Write([]byte(`{"type":"success","value": {` +
				`"joke": "##firstname## ##lastname## has never been in a fight, ever."}}`))
		},
	))
	if jokeAPIResponse != nil {
		jokeAPIServer = httptest.NewServer(http.HandlerFunc(jokeAPIResponse))
	}
	js.jokeAPI = jokeAPIServer.URL

	// run Joke Server
	stop := js.Run()

	// return if /ping is ready
	for {
		response, err := http.Get(fmt.Sprintf("http://127.0.0.1%s/ping", listen))
		if err != nil {
			continue
		}
		defer response.Body.Close()
		if response.StatusCode == 200 && err == nil {
			break
		}
		time.Sleep(time.Second)
	}

	// return the port and teardown function
	return listen, &reqCount, func(t *testing.T) {
		stop()
		nameAPIServer.Close()
		jokeAPIServer.Close()
	}
}

func TestJokeAPI(t *testing.T) {
	t.Parallel()

	listen, reqCount, teardown := setup(t, 100, nil, nil)
	defer teardown(t)

	// Testing /v1/joke
	response, err := http.Get(fmt.Sprintf("http://127.0.0.1%s/v1/joke", listen))
	assert.Equal(t, 200, response.StatusCode)
	assert.NoError(t, err)

	defer response.Body.Close()
	contents, err := ioutil.ReadAll(response.Body)
	assert.NoError(t, err)

	c := string(contents)
	assert.Contains(t, c, "Anastacio Jr. has never been in a fight, ever.")

	// the total request count should be 3
	time.Sleep(50 * time.Millisecond)
	assert.Equal(t, int64(3), atomic.LoadInt64(reqCount))
}

func TestRootPath(t *testing.T) {
	t.Parallel()

	listen, reqCount, teardown := setup(t, 100, nil, nil)
	defer teardown(t)

	// Testing root path
	response, err := http.Get(fmt.Sprintf("http://127.0.0.1%s/", listen))
	assert.Equal(t, 200, response.StatusCode)
	assert.NoError(t, err)

	defer response.Body.Close()
	contents, err := ioutil.ReadAll(response.Body)
	assert.NoError(t, err)

	c := string(contents)
	assert.Contains(t, c, "Anastacio Jr. has never been in a fight, ever.")

	// the total request count should be 3
	time.Sleep(50 * time.Millisecond)
	assert.Equal(t, int64(3), atomic.LoadInt64(reqCount))
}

func TestPrometheus(t *testing.T) {
	t.Parallel()

	listen, _, teardown := setup(t, 100, nil, nil)
	defer teardown(t)

	resp, err := http.Get(fmt.Sprintf("http://127.0.0.1%s/", listen))
	assert.NoError(t, err)
	assert.Equal(t, 200, resp.StatusCode)
	defer resp.Body.Close()

	// Testing /metrics
	response, err := http.Get(fmt.Sprintf("http://127.0.0.1%s/metrics", listen))
	assert.Equal(t, 200, response.StatusCode)
	assert.NoError(t, err)

	defer response.Body.Close()
	contents, err := ioutil.ReadAll(response.Body)
	assert.NoError(t, err)

	c := string(contents)
	assert.Contains(t, c, "joke_joke_api_request_latency_ms")
	assert.Contains(t, c, "joke_name_api_request_latency_ms")
}

func TestRateLimit(t *testing.T) {
	t.Parallel()

	listen, _, teardown := setup(t, 1, nil, nil)
	defer teardown(t)

	// This is rate limited since there's another ping already processed
	response, err := http.Get(fmt.Sprintf("http://127.0.0.1%s/ping", listen))
	assert.NoError(t, err)
	assert.Equal(t, 429, response.StatusCode)
	defer response.Body.Close()
}

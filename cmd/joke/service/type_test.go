package service

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPerson(t *testing.T) {
	tests := map[string]struct {
		json      string
		expected  Person
		errExists bool
	}{
		"normal": {
			json: `{"name": "Mattie Steuber", "other": "foobar"}`,
			expected: Person{
				Name:      "Mattie Steuber",
				FirstName: "Mattie",
				LastName:  "Steuber",
			},
		},
		"middle_name": {
			json: `{"name": "Chun Chi Kwong"}`,
			expected: Person{
				Name:      "Chun Chi Kwong",
				FirstName: "Chun",
				LastName:  "Kwong",
			},
		},
		"empty": {
			json:     `{}`,
			expected: Person{},
		},
		"invalid": {
			json:      `{`,
			errExists: true,
		},
	}
	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			var person Person
			err := json.Unmarshal([]byte(test.json), &person)
			if test.errExists {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
				assert.Equal(t, test.expected, person)
			}
		})
	}
}

func TestJokeResult(t *testing.T) {
	tests := map[string]struct {
		json     string
		expected JokeResult
	}{
		"normal": {
			json: `{"type":"success","value": {"joke": "foobar"}}`,
			expected: JokeResult{
				JokeType: "success",
				Value: JokeValue{
					Joke: "foobar",
				},
			},
		},
	}
	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			var jr JokeResult
			err := json.Unmarshal([]byte(test.json), &jr)
			assert.NoError(t, err)
			assert.Equal(t, test.expected, jr)
		})
	}
}

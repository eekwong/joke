package service

import (
	"context"
	"net/http"
	"sync"
	"time"

	"github.com/rs/zerolog/log"
	"gitlab.com/eekwong/joke/pkg/lifecycle"
)

// JokeServer represents our Joke API server
type JokeServer struct {
	lifecycle.Service
	server  *http.Server
	cached  int
	workers int
	jokes   chan string
	nameAPI string
	jokeAPI string
}

// NewJokeServer creates new JokeServer instance
func NewJokeServer(
	listen string, cached, workers, ratelimit int,
	oauthDomain, oauthClientID, oauthClientSecret string,
	verbose bool) *JokeServer {
	if cached > 500 {
		cached = 500
	} else if cached < 1 {
		cached = 1
	}
	if workers > (cached / 2) {
		workers = cached / 2
	}
	if workers < 1 {
		workers = 1
	}

	js := &JokeServer{
		cached:  cached,
		workers: workers,
		jokes:   make(chan string, cached),
	}
	js.server = &http.Server{
		Addr: listen,
		Handler: setupChiRouter(
			js.jokes, ratelimit, oauthDomain, oauthClientID, oauthClientSecret, verbose),
	}
	return js
}

func (js *JokeServer) cachingJokes(stopCh <-chan struct{}, wg *sync.WaitGroup) {
	// go-routine to prefetch a number of Person
	for i := 0; i < js.workers; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()

			for {
				select {
				case <-stopCh:
					return
				default:
					joke, err := GetFunnyJoke("", "", js.nameAPI, js.jokeAPI)
					if err == nil {
						select {
						case <-stopCh:
							return
						case js.jokes <- joke:
						}
					}
				}
			}
		}()
	}
}

// Run is to run the service.
func (js *JokeServer) Run() func() {
	var wg sync.WaitGroup

	// server go-routine
	wg.Add(1)
	go func() {
		defer wg.Done()

		if err := js.server.ListenAndServe(); err != nil {
			if err != http.ErrServerClosed {
				log.Error().
					Err(err).
					Msg("error in http.Server.ListenAndServe")
			}
		}
	}()

	stopCh := make(chan struct{})

	js.cachingJokes(stopCh, &wg)

	log.Info().Msg("joke server started")

	return func() {
		close(stopCh)

		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		if err := js.server.Shutdown(ctx); err != nil {
			log.Error().
				Err(err).
				Msg("error in shutting down HTTP server")
		}
		wg.Wait()

		log.Info().Msg("joke server stopped")
	}
}

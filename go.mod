module gitlab.com/eekwong/joke

go 1.12

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/didip/tollbooth v4.0.2+incompatible
	github.com/didip/tollbooth_chi v0.0.0-20170928041846-6ab5f3083f3d
	github.com/facebookarchive/freeport v0.0.0-20150612182905-d4adf43b75b9
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-chi/jwtauth v4.0.3+incompatible
	github.com/go-chi/render v1.0.1
	github.com/google/uuid v1.1.1
	github.com/gorilla/sessions v1.1.1
	github.com/markbates/goth v1.59.0
	github.com/patrickmn/go-cache v2.1.0+incompatible // indirect
	github.com/prometheus/client_golang v1.2.1
	github.com/rs/zerolog v1.16.0
	github.com/spf13/cobra v0.0.5
	github.com/stretchr/testify v1.3.0
	github.com/swaggo/http-swagger v0.0.0-20190614090009-c2865af9083e
	github.com/swaggo/swag v1.6.3
	github.com/tevino/abool v0.0.0-20170917061928-9b9efcf221b5
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0 // indirect
)

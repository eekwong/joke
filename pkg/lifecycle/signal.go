package lifecycle

import (
	"os"
	"os/signal"
	"syscall"

	"github.com/rs/zerolog/log"
)

func listenToSignals() chan struct{} {
	sigs := make(chan os.Signal, 1)
	done := make(chan struct{})
	signal.Notify(sigs,
		syscall.SIGABRT, syscall.SIGILL, syscall.SIGINT, syscall.SIGTERM, syscall.SIGSEGV)
	go func() {
		sig := <-sigs
		log.Info().
			Str("signal", sig.String()).
			Msg("signal caught")
		close(done)
	}()
	return done
}

VERSION = 0.1.0

UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Linux)
	LDFLAGS = '-extldflags "-static"'
endif

.PHONY: help
help:
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

.PHONY: generate
generate:
	@echo "+ $@"
	cd cmd/joke; go generate; cd -

.PHONY: test
test: generate			## Run golint, staticcheck, and go test for all the sub-directories
	@echo "+ $@"
	go mod tidy
	@if [ "$$(command -v revive)" ]; then \
			echo "revive ./..."; \
			revive ./...; \
			test -z "$$(revive ./...)"; \
	else \
			echo "golint -set_exit_status ./..."; \
			golint -set_exit_status ./...; \
	fi
	@if [ -x "$$(command -v staticcheck)" ]; then \
			echo "staticcheck ./..."; \
			staticcheck ./...; \
	fi
	go vet ./...
	go test $(TAGS) -v -race -cover -count=1 ./...

.PHONY: clean
clean:				## Clean all artifacts
	@echo "+ $@"
	rm -fr dist
	rm -fr cmd/joke/docs

.PHONY: build
build: generate			## Build joke binary
	@echo "+ $@"
	go build -v -a \
		--ldflags "$(LDFLAGS) -X gitlab.com/eekwong/joke/cmd/joke/cmd.Version=$(VERSION)" \
		-o dist/joke gitlab.com/eekwong/joke/cmd/joke

.PHONY: run
run: generate			## go run the Joke service
	@echo "+ $@"
	go run gitlab.com/eekwong/joke/cmd/joke server || true

.PHONY: image
image:				## Build joke docker image
	@echo "+ $@"
	docker build -f build/Dockerfile -t registry.gitlab.com/eekwong/joke:latest ./

.PHONY: deploy-docker-up
deploy-docker-up: image		## Deploy joke using docker-compose
	@echo "+ $@"
	docker-compose -f deployments/docker/docker-compose.yml up -d

.PHONY: deploy-docker-down
deploy-docker-down:		## Stop job docker-compose setup
	@echo "+ $@"
	docker-compose -f deployments/docker/docker-compose.yml down -v

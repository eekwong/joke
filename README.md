# Joke

A go project to make a joke using https://en.namefake.com/api and http://www.icndb.com/api/.

## Trying it out

Pre-requisite:

docker: https://docs.docker.com/install/

### Single joke in the terminal:

`docker run --rm registry.gitlab.com/eekwong/joke /joke single`

### Joke server for web

`docker run -it --rm -p 8080:8080 registry.gitlab.com/eekwong/joke`

and visit

1. http://localhost:8080/ for a joke
2. http://localhost:8080/swagger/index.html for the API swagger UI
3. http://localhost:8080/metrics for a prometheus scrape endpoint

## Building the source

Pre-requisite:

go 1.12+: https://golang.org/doc/install#install
swag: go get -u github.com/swaggo/swag/cmd/swag

Steps:

1. git clone https://gitlab.com/eekwong/joke.git
2. cd joke
3. make build

The executable is in the `dist` folder.

## Building the docker image

Pre-requisite:
docker

`make image` will create an image named `registry.gitlab.com/eekwong/joke:latest`.

## Other Makefile targets

`make` will display a menu for all the targets

`make deploy-docker-up` will deploy a Joke server along with Prometheus and Grafana for metrics.

`make deploy-docker-down` is to teardown the docker-compose setup.

## Protecting the api using Google SSO

Get oauth client ID and secret from Google credential page:
https://console.cloud.google.com/apis/credentials

Authorized domain needs to have a hostname, 127.0.0.1.nip.io is a trick to use IP as a domain name.

Callback URL is the domain with path: `/auth/google/callback`

Launch the joke server with arguments e.g.

`dist/joke server --sso-domain "127.0.0.1.nip.io:8080" --sso-client-id "<id>" --sso-client-secret "<secret>"`

Visit http://127.0.0.1.nip.io:8080 will redirect you to the google server to authenticate.

Once you got the joke, there is a JWT for you to curl using the JWT

`curl -H "Authorization: BEARER <JWT>" http://127.0.0.1.nip.io:8080/v1/joke`
